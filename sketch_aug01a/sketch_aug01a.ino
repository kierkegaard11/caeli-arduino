
#include <Arduino.h>
#include "Adafruit_SHT31.h"
#include <SoftwareSerial.h>
#include <MQ135.h>
Adafruit_SHT31 sht31 = Adafruit_SHT31();
SoftwareSerial mySerial(6,7);
  
String host = "http://1ee9-87-116-153-197.ngrok.io";
int ppm = -1;
float temperature = -1;
float humidity = -1;
String licence = "Caelikrmc5j02";
MQ135 gasSensor = MQ135(0);

void setup() {
  mySerial.begin(9600);
  delay(700);
  Serial.begin(9600);
  delay(700);
  //updateSerial();

   
  mySerial.println("AT"); //Once the handshake test is successful, it will back to OK
  delay(1000);
  updateSerial();
  mySerial.println("AT+CSQ"); //Signal quality test, value range is 0-31 , 31 is the best
  delay(1000);
  updateSerial();
  mySerial.println("AT+CREG?"); //Check whether it has registered in the network
  delay(1000);
  updateSerial();
  
  Serial.println("SHT31 test");
  if (! sht31.begin(0x44)) {
  Serial.println("Couldn't find SHT31");
  }
}

void loop() {
  
  ppm = gasSensor.getPPM();
  temperature = sht31.readTemperature();
  humidity = sht31.readHumidity();
  
  Serial.println("");
  Serial.print("PPM particles: ");
  Serial.println(ppm);
  Serial.print("Temperature in C: ");
  Serial.println(temperature);
  Serial.print("Humidity in %: ");
  Serial.println(humidity); 
  delay(3000);
  
  sendState();
  delay(10000);
}

void updateSerial(){
  delay(500);
  while(mySerial.available()) {
    Serial.write(mySerial.read());//Forward what Software Serial received to Serial Port
  }
}

void sendState(){

mySerial.println("AT"); //Once the handshake test is successful, it will back to OK
  delay(1000);
  updateSerial();
  mySerial.println("AT+CSQ"); //Signal quality test, value range is 0-31 , 31 is the best
  delay(1000);
  updateSerial();
  mySerial.println("AT+CREG?"); //Check whether it has registered in the network
  delay(1000);
  updateSerial();
  
  mySerial.println("AT+SAPBR=3,1,Contype,GPRS");
  delay(2000);
  updateSerial();
  mySerial.println("AT+CSTT=iot.1nce.net,\"\",\"\"");
  delay(2000);
  updateSerial();
  mySerial.println("AT+SAPBR=1,1");
  delay(2000);
  updateSerial();
  Serial.println("Sending request via GSM...");
  
  String request = host;
  request += "/caeli/devices/states?";
  request += "l=";
  request += licence;
  request += "&a=";
  request += (int)ppm;
  request += "&h=";
  request += (int)humidity;
  request += "&t=";
  request += (int)temperature;
  
  //String apn = "iot.1nce.net";
  
  String fullCommand = "AT+HTTPPARA=URL,";
  fullCommand+= request;
  
  mySerial.println("AT+HTTPINIT");
  delay(3000);
  updateSerial();
  
  mySerial.println("AT+HTTPPARA=CID,1");
  delay(3000);
  updateSerial();
  
  mySerial.println(fullCommand);
  delay(3000);
  updateSerial();
  
  mySerial.println("AT+HTTPACTION=0");
  delay(5000);
  updateSerial();
   
  mySerial.println("AT+HTTPREAD");
  delay(3000);
  updateSerial();
  
  
  if (mySerial.available()) {
    mySerial.read();
    mySerial.flush();
  }
  
  delay(1000);
  
  Serial.print("Request sent (");
  Serial.print(request);
  Serial.println(")");
  
  String response;
  
  if (mySerial.available() > 0) {
    response = mySerial.readString();
    Serial.println("Data received!");
    Serial.println(response);
    Serial.println();
  }
  
  mySerial.println("AT+HTTPTERM");
  updateSerial();
  delay(3000);
}
